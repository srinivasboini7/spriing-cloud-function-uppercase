package com.srini.lamda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.function.Function;
import java.util.function.Supplier;

@SpringBootApplication
public class LamdaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LamdaApplication.class, args);
	}


	@Bean
	public Function<String, String> getDetails(){
		return String::toUpperCase;
	}

}
